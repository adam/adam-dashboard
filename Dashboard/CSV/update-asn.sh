#!/bin/sh
set -e
echo 'asn;as_name;as_country' > asn_2.csv
curl -s 'https://ftp.ripe.net/ripe/asnames/asn.txt' | sed -E 's/ /;/; s/, ([[:upper:]]{2,2})$/;\1/' >> asn_2.csv
mv asn_2.csv asn.csv