import json
from datetime import datetime, timedelta
from functools import reduce
from os import environ
from sys import exit, stderr

import requests
from requests.models import PreparedRequest

API_TOKEN = environ.get("API_TOKEN", None)
API_URL = "https://stats.nic.cz"
ZONE = "cz"
JSONSCHEMA_VERSION = "v2.1"


def get_json(query, zone=ZONE):
    req = PreparedRequest()
    if zone:
        req.prepare_url(f"{API_URL}/{query}", {"zone": f"eq.{ZONE}"})
    else:
        req.url = f"{API_URL}/{query}"
    req.headers = {"Authorization": f"Bearer {API_TOKEN}"}
    req.method = "GET"
    s = requests.Session()
    try:
        r = s.send(req)
        return r.json()
    except (json.decoder.JSONDecodeError) as e:
        stderr.write(f"Error fetching data:\n{query}\n{r.text}\n{str(e)}")
        exit(1)


def get_ts():
    ts1 = get_json("/fred_domains_by_cc_latest?select=ts&limit=1")[0]["ts"]
    ts2 = get_json("/fred_domains_by_state_latest?select=ts&limit=1")[0]["ts"]
    ts3 = get_json("/fred_dnssec_domains_latest")[0]["ts"]
    if ts1 != ts2 or ts2 != ts3:
        stderr.write("Timestamps don't match:\n" +
                     f"/fred_domains_by_cc_latest: {ts1}\n" +
                     f"/fred_domains_by_state_latest: {ts2}\n" +
                     f"/fred_dnssec_domains_latest: {ts3}")
        exit(1)
    return datetime.strptime(ts1.split("T")[0], "%Y-%m-%d").strftime("%Y-%m-%d")


if __name__ == "__main__":
    if not API_TOKEN:
        stderr.write("Error: API_TOKEN not set")
        exit(1)

    dist_data = get_json("/fred_domains_by_cc_latest?select=code:cc,value:domains")
    for obj in dist_data:
        if obj['code'] is None:
            dist_data.append({'code': 'unknown', 'value': obj['value']})
            dist_data.remove(obj)

    data = {
        "timestamp": get_ts(),
        "tld": ZONE,
        "version": JSONSCHEMA_VERSION,
        "distribution_data": dist_data,
        "total": reduce(lambda a, b: dict(a, **b), [{j["state"]: j["domains"]} for j in get_json("/fred_domains_by_state_latest?select=state,domains")]),
        "event": None
    }

    yesterday = (datetime.now() - timedelta(days=1)).strftime("%Y-%m-%d")
    data["total"]["dnssec_enabled"] = get_json("/fred_dnssec_domains_latest?select=domains")[0]["domains"]
    data["total"]["idn"] = len(get_json(f"/domain_ranking?ts=gte.{yesterday}&domain=like.xn--*&select=domain", zone=None))

    print(json.dumps(data))
