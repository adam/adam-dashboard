import yaml
import glob
import json
import re

languages = ["en", "cs"]
out = {}

for language in languages:
    out[language] = []
    for filename in glob.glob(rf"docs/{language}/*[Rr]md"):
        rmd_file = open(filename, "r")
        next(rmd_file)
        rmd_header = ""
        for line in rmd_file:
            if line.startswith("---"):
                break
            rmd_header += line
        yml_header = yaml.safe_load(rmd_header)
        out[language].append({"url": "../" + re.sub(r"[Rr]md$", "html", filename), "title": yml_header["title"]})

print(json.dumps(out, ensure_ascii=False))
