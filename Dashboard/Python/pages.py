import yaml
import json
from sys import argv
from os import path

dir = path.dirname(argv[1])
languages = ["en", "cs"]
pages = json.load(open(argv[1], "r"))

for page in pages:
    page["title"] = {}
    for language in languages:
        rmd_file = open(f"{dir}/{language}/{page['file']}", "r")
        next(rmd_file)
        rmd_header = ""
        for line in rmd_file:
            if line.startswith("---"):
                break
            rmd_header += line
        yml_header = yaml.safe_load(rmd_header)
        page["title"][language] = yml_header["title"]

json.dump(pages, open(argv[1], "w"), ensure_ascii=False)
