#! /usr/bin/env python
"""Poor-man's tangle for R Markdown"""

import os
import sys

def next_chunk(text, offset=0):
    """Return start and end of next R chunk, beginning from `offset`.

    ``None`` is returned if no chunk is found.
    """
    bticks = text.find("```{r", offset)
    if bticks == -1:
        return None
    stop = min(text.find(",", bticks), text.find("}", bticks))
    cname = text[bticks + 6:stop].strip()
    start = text.find("}", bticks) + 1
    eticks = text.find("```", start)
    end = eticks - 1 if text[eticks-1] == "\n" else eticks
    return (cname, start, end)

if __name__ == '__main__':
    rmd = open(sys.argv[1])
    bn = os.path.splitext(os.path.basename(sys.argv[1]))[0]
    if not os.path.exists("R"):
        os.makedirs("R")
    intext = rmd.read()
    offset = 0
    while True:
        lims = next_chunk(intext, offset)
        if lims is None:
            break
        out = open("R/" + bn + "_" + lims[0] + ".R", "w")
        out.write(intext[lims[1]:lims[2]].strip() + "\n")
        out.close()
        offset = lims[2]
