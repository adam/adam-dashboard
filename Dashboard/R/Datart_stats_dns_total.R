endpoint <- api_endpoint("/datart_qps_total_1h",
  ts = get_ts(months, 12)
)
qps_total_1h <- fromJSONwithToken(endpoint)


qps_total_plot(qps_total_1h, endpoint)
