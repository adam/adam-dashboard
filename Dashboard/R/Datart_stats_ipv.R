endpoint <- "/datart_qps_ipv_1h"

qps_ipv_1h <- fromJSONwithToken(api_endpoint(endpoint,
  ts = get_ts(months, 6)
))

qps_ipv_plot(qps_ipv_1h, endpoint)
