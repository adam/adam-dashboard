qps_qtype_1h <- fromJSONwithToken(api_endpoint("/datart_qps_qtype_1h",
  ts = get_ts(days, 30)
)) %>%
  rename(Value = qtype) %>%
  mutate(Value = as.factor(Value))

qtypes <- read.csv("../../Dashboard/CSV/qtypes.csv", sep = ",")


qps_qtype_plot2(qps_qtype_1h, qtypes, 4, 5)
