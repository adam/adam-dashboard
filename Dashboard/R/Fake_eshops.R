source("../R/common.R")
source("../R/fromJSONwithToken.R")
library(stringr)


fake_domains_regdate <- fromJSONwithToken(
  api_endpoint("/crawler_fake_eshop_2w",
    ts = get_ts(days, 14)
  ),
) %>%
  filter(predicted == "likely-fake") %>%
  mutate(current_registration_date = as.Date(current_registration_date)) %>%
  group_by(current_registration_date, domain) %>%
  summarise(n = n(), detection_date = max(as.Date(ts))) %>%
  select(-n)


fake_domains <- fromJSONwithToken(
  api_endpoint("/crawler_fake_eshop_2w_export",
    last_seen = get_ts(days, 14)
  ),
) %>%
  mutate(
    first_seen = as.Date(first_seen),
    last_seen = as.Date(last_seen),
    class = ifelse(min_prob > 0.65, gettext("very likely"), gettext("likely"))
  ) %>%
  select(-last_seen, -min_prob, -max_prob) %>%
  left_join(fake_domains_regdate, by = c("domain"))


df_ranking <- fromJSONwithToken(
  api_endpoint("/domain_ranking",
    ts = get_ts(days, 14),
    domain = paste0(
      "in.(",
      paste(as.vector(fake_domains$domain), collapse = ","),
      ")"
    ),
    select = "ts,domain,registrar_id,queries"
  ),
) %>%
  mutate(ts = as.Date(ts)) %>%
  filter(queries != 0) %>%
  group_by(ts, domain, registrar_id) %>%
  summarise(queries_24h = sum(queries)) %>%
  group_by(domain, registrar_id) %>%
  summarise(
    tsmax = max(ts),
    mean_queries = round(mean(queries_24h), 0)
  ) %>%
  filter(tsmax == max(tsmax)) %>%
  ungroup()

df_fake_ranking <- inner_join(fake_domains, df_ranking, by = c("domain")) %>%
  mutate(
    registrar_id = str_remove_all(registrar_id, "REG-"),
    queries_tile = adam_color_tile(
      mean_queries,
      "white", dashboard_colors[1]
    ),
    class = cell_spec(class,
      bold = TRUE,
      color = ifelse(class == gettext("very likely"),
        dashboard_colors[3],
        dashboard_colors[1]
      )
    ),
    dt_url = paste0("https://www.nic.cz/whois/domain/", domain),
    domain_link = cell_spec(domain, "html", link = dt_url, new_tab = TRUE)
  )

df_number <- fromJSONwithToken(
  api_endpoint("/domain_ranking",
    ts = get_ts(days, 14),
    domain = paste0(
      "in.(",
      paste(as.vector(fake_domains$domain), collapse = ","),
      ")"
    ),
    select = "ts,domain,registrar_id,queries"
  ),
) %>%
  mutate(
    ts = as.Date(ts),
    registrar_id = str_remove_all(registrar_id, "REG-")
  ) %>%
  group_by(domain, registrar_id) %>%
  summarise(n = n()) %>%
  filter(n == 1) %>%
  mutate(ts = as.Date(Sys.Date() - 7), queries_24h = 0) %>%
  select(ts, domain, registrar_id, queries_24h)

df_traffic_spark <- fromJSONwithToken(
  api_endpoint("/domain_ranking",
    ts = get_ts(days, 14),
    domain = paste0(
      "in.(",
      paste(as.vector(fake_domains$domain), collapse = ","),
      ")"
    ),
    select = "ts,domain,registrar_id,queries"
  ),
) %>%
  mutate(
    ts = as.Date(ts),
    registrar_id = str_remove_all(registrar_id, "REG-")
  ) %>%
  group_by(ts, domain, registrar_id) %>%
  summarise(queries_24h = sum(queries)) %>%
  rbind(df_number) %>%
  arrange(ts) %>%
  group_by(domain, registrar_id) %>%
  summarise(
    queries_bar = spk_chr(
      queries_24h,
      type = "line",
      lineColor = sparkline_colors[1],
      fillColor = sparkline_colors[2],
      spotColor = TRUE,
      width = 80
    )
  ) %>%
  ungroup()

df_fake_ranking_sparklines <- left_join(df_traffic_spark, df_fake_ranking,
  by = c("domain", "registrar_id")
) %>%
  filter(!is.na(first_seen))


htmltools::attachDependencies(
  htmltools::div(),
  html_dependency_sparkline()
)

df_fake_ranking_sparklines %>%
  arrange(desc(mean_queries)) %>%
  select(
    domain, registrar_id, detection_date,
    current_registration_date, class, queries_tile, queries_bar
  ) %>%
  kable(
    "html",
    table.attr = "data-search-cols='1'",
    escape = FALSE,
    format.args = list(big.mark = " "),
    align = c("l", "l", "c", "c", "c", "c", "c"),
    col.names = c(
      gettext("Domain"),
      gettext("Registrar"),
      gettext("Detected"),
      gettext("Registered"),
      gettext("Level"),
      gettext("QPD"),
      gettext("Trend")
    )
  ) %>%
  kable_styling(
    bootstrap_options = c("hover", "condensed"),
    full_width = TRUE,
    htmltable_class = paste(
      "adam-table-search",
      "adam-table-scroll",
      "adam-table-nowrap",
      "dtable",
      "table-condensed"
    )
  )
