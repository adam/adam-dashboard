source("../R/common.R")
source("../R/fromJSONwithToken.R")


data_eshop <- fromJSONwithToken(
  api_endpoint("/crawler_fake_eshop_2w_export_all",
    first_seen = get_ts(months, 3)
  )
)


fake_eshop <- data_eshop %>%
  mutate(
    first_seen = as.Date(first_seen),
    last_seen = as.Date(last_seen),
    len = last_seen - first_seen
  ) %>%
  group_by(first_seen, predicted) %>%
  summarise(
    n = n(),
    avg_life = round(mean(len), 2)
  ) %>%
  mutate(total = sum(n)) %>%
  ungroup() %>%
  spread(predicted, n)



plot_ly(fake_eshop,
  x = ~first_seen,
  y = ~`likely-fake`,
  name = gettext("likely fake"),
  type = "scatter",
  mode = "none",
  stackgroup = "one",
  hovertemplate = paste0(
    "<b>", gettext("likely fake"), "</b>:",
    fake_eshop$`likely-fake`,
    " (", round((fake_eshop$`likely-fake` / fake_eshop$total) * 100, 2),
    "%)", "<br><extra></extra>"
  ),
  fillcolor = dashboard_pal(5)[3]
) %>%
  add_trace(
    y = ~`likely-not-fake`,
    name = gettext("likely not fake"),
    hovertemplate = paste0(
      "<b>", gettext("likely not fake"), "</b>:",
      fake_eshop$`likely-not-fake`,
      " (", round((fake_eshop$`likely-not-fake` / fake_eshop$total) * 100, 2),
      "%)", "<br><extra></extra>"
    ),
    fillcolor = dashboard_pal(5)[1]
  ) %>%
  add_trace(
    y = ~http_error,
    name = gettext("http error"),
    hovertemplate = paste0(
      "<b>", gettext("http error"), "</b>:",
      fake_eshop$http_error,
      " (", round((fake_eshop$http_error / fake_eshop$total) * 100, 2),
      "%)", "<br><extra></extra>"
    ),
    fillcolor = dashboard_pal(5)[2]
  ) %>%
  add_trace(
    y = ~no_content,
    name = gettext("no content"),
    hovertemplate = paste0(
      "<b>", gettext("no content"), "</b>:",
      fake_eshop$no_content,
      " (", round((fake_eshop$no_content / fake_eshop$total) * 100, 2),
      "%)", "<br><extra></extra>"
    ),
    fillcolor = dashboard_pal(5)[4]
  ) %>%
  add_trace(
    y = ~`not-an-eshop`,
    name = gettext("not an e-shop"),
    hovertemplate = paste0(
      "<b>", gettext("not an e-shop"), "</b>:",
      fake_eshop$`not-an-eshop`,
      " (", round((fake_eshop$`not-an-eshop` / fake_eshop$total) * 100, 2),
      "%)", "<br><extra></extra>"
    ),
    fillcolor = dashboard_pal(5)[5]
  ) %>%
  config(displayModeBar = TRUE, locale = locale_lang_code()) %>%
  layout(
    hovermode = "x unified",
    margin = list(t = 75),
    title = gettext("e-shop classification"),
    legend = list(
      orientation = "h",
      xanchor = "center",
      x = 0.5,
      y = 1.08
    ),
    separators = ".\u202F",
    xaxis = list(
      title = gettext("Date"),
      rangeselector = list(
        buttons = list(
          list(
            count = 14,
            label = gettext("14 d"),
            step = "day",
            stepmode = "backward"
          ),
          list(
            count = 1,
            label = gettext("1 m"),
            step = "month",
            stepmode = "backward"
          ),
          list(
            count = 3,
            label = gettext("3 m"),
            step = "month",
            stepmode = "backward"
          ),
          list(
            step = "all",
            label = gettext("all")
          )
        ),
        rangeslider = list(type = "date")
      )
    ),
    autosize = TRUE,
    yaxis = list(
      title = gettext("Number of domains"),
      tickformat = ",d"
    )
  )
