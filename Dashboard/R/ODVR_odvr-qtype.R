odvr_qtype_1h <- fromJSON(api_endpoint("/odvr_qps_qtype_1h",
  ts = get_ts(days, 30)
))

odvr_qtype_1h <- odvr_qtype_1h %>%
  rename(Value = qtype) %>%
  mutate(Value = as.factor(Value))
qtypes <- read.csv("../../Dashboard/CSV/qtypes.csv", sep = ",")

odvr_qtype <- odvr_qtype_1h %>% inner_join(qtypes, by = c("Value"))

top_qtypes_latest <- odvr_qtype %>%
  group_by(ts, TYPE) %>%
  summarise(qps = sum(qps)) %>%
  mutate(TYPE = as.factor(TYPE)) %>%
  mutate(TYPE = fct_lump_n(TYPE,
    n = 3, w = qps,
    other_level = gettext("Other")
  )) %>%
  group_by(ts, TYPE) %>%
  summarise(qps = sum(qps))

df_odvr_qtypes <- odvr_qtype %>%
  mutate(ts = as.Date(ts)) %>%
  mutate(TYPE = as.factor(TYPE), queries = qps * 3600) %>%
  group_by(ts, TYPE) %>%
  summarise(queries = sum(queries)) %>%
  inner_join(top_qtypes_latest, by = c("TYPE")) %>%
  select(ts.x, TYPE, queries) %>%
  rename(ts = ts.x)


df_odvr_qtype_traffic <- odvr_qtype %>%
  mutate(ts = as.Date(ts)) %>%
  mutate(TYPE = as.factor(TYPE), queries = qps * 3600) %>%
  group_by(ts, TYPE) %>%
  summarise(queries = sum(queries)) %>%
  anti_join(top_qtypes_latest, by = c("TYPE")) %>%
  mutate(TYPE = gettext("Other")) %>%
  group_by(ts, TYPE) %>%
  summarise(queries = sum(queries)) %>%
  bind_rows(df_odvr_qtypes) %>%
  mutate(queries_24h = queries / 86400)


df_odvr_qtype_traffic_table <- df_odvr_qtype_traffic %>%
  group_by(TYPE) %>%
  summarise(
    qps_max = round(max(queries_24h), 0),
    qps_min = round(min(queries_24h), 0)
  ) %>%
  ungroup() %>%
  mutate(
    qps_max_tile = adam_color_tile(qps_max, "white", dashboard_colors[1]),
    qps_min_tile = adam_color_tile(qps_min, "white", dashboard_colors[1])
  ) %>%
  inner_join(
    df_odvr_qtype_traffic %>%
      group_by(TYPE) %>%
      summarise(
        qps_current = mean(queries_24h)
      ) %>%
      ungroup() %>%
      mutate(
        qps_current_tile = adam_color_tile(
          qps_current,
          "white", dashboard_colors[3]
        )
      ),
    by = c("TYPE")
  )

df_odvr_qtype_traffic_spark <- df_odvr_qtype_traffic %>%
  group_by(ts, TYPE) %>%
  summarise(qps = mean(queries_24h)) %>%
  arrange(ts) %>%
  group_by(TYPE) %>%
  summarise(
    queries_bar = spk_chr(
      qps,
      type = "line",
      lineColor = sparkline_colors[1],
      fillColor = sparkline_colors[2],
      spotColor = TRUE,
      width = 80
    )
  )

htmltools::attachDependencies(
  htmltools::div(), html_dependency_sparkline()
)

df_odvr_qtype_traffic_table %>%
  left_join(df_odvr_qtype_traffic_spark, by = "TYPE") %>%
  arrange(desc(qps_current)) %>%
  select(
    TYPE,
    qps_max_tile,
    qps_min_tile,
    queries_bar,
    qps_current_tile
  ) %>%
  kable(
    "html",
    table.attr = "class='dtable'",
    escape = FALSE,
    align = c("r", "c", "c", "c", "c", "c"),
    format.args = list(big.mark = " "),
    col.names = c(
      "QTYPE", "Max QPS", "Min QPS",
      "Trend", gettext("Average")
    )
  ) %>%
  column_spec(1, bold = TRUE, width = "3cm") %>%
  column_spec(2, width = "3cm") %>%
  column_spec(3, width = "3cm") %>%
  column_spec(4, width = "3cm") %>%
  column_spec(5, width = "3cm") %>%
  kable_styling(
    bootstrap_options = c("striped", "hover", "condensed", "responsive"),
    full_width = TRUE,
    position = "left"
  )
