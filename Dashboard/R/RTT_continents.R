df_rtt <- fromJSON(api_endpoint("/cz_rtt_cc_server_ipv_1d",
  ts = get_ts(months, 9)
))
df_qps <- fromJSONwithToken(api_endpoint("/cz_qps_cc_server_ipv_1d",
  ts = get_ts(months, 9)
))

df_rtt_qps <- left_join(df_qps, df_rtt) %>%
  mutate(
    country_code = ifelse(is.na(country_code), "Missing", country_code),
    region = countrycode::countrycode(country_code, "iso2c", "region23",
      custom_match = c(
        XK = "Eastern Europe",
        TF = "Melanesia",
        IO = "Southern Asia",
        GS = "South America",
        CX = "Australia and New Zealand",
        CC = "South-Eastern Asia",
        BV = "Southern Africa",
        AQ = "Antarctica"
      )
    ),
    continent = countrycode::countrycode(country_code, "iso2c", "continent",
      custom_match = c(
        XK = "Europe",
        TF = "Oceania",
        IO = "Asia",
        GS = "Americas",
        CX = "Oceania",
        CC = "Asia",
        BV = "Africa",
        AQ = "Antarctica"
      )
    )
  )

df_rtt_qps_continent <- df_rtt_qps %>%
  mutate(ts = as_datetime(ts, tz = "Europe/Prague")) %>%
  group_by(ts, continent) %>%
  summarise(
    N = n(),
    total_qps = sum(qps),
    rtt = weighted.mean(mean_median_rtt, qps,
      na.rm = TRUE
    ),
    SE.low = rtt - (sd(mean_median_rtt, na.rm = TRUE) / sqrt(N)),
    SE.high = rtt + (sd(mean_median_rtt, na.rm = TRUE) / sqrt(N))
  ) %>%
  mutate(
    continent = ifelse(continent == "Oceania",
      gettext("Oceania"),
      ifelse(continent == "Americas",
        gettext("Americas"),
        ifelse(continent == "Europe",
          gettext("Europe"),
          ifelse(continent == "Asia",
            gettext("Asia"),
            ifelse(continent == "Africa",
              gettext("Africa"), continent
            )
          )
        )
      )
    )
  ) %>%
  filter(!is.na(continent)) %>%
  ungroup()

fig <- df_rtt_qps_continent %>%
  plot_ly() %>%
  add_trace(
    x = ~ts,
    y = ~rtt,
    color = ~continent,
    colors = dashboard_colors,
    hovertemplate = ~ paste0(
      "<br><b>QPS: </b>",
      round(total_qps, 2), "<br><b>RTT: </b>",
      round(rtt, 2), "ms</B>"
    ),
    type = "scatter",
    mode = "lines",
    showlegend = TRUE
  ) %>%
  layout(
    hovermode = "x unified",
    xaxis = list(
      title = gettext("Date"),
      rangeselector = list(
        buttons = list(
          list(
            count = 7,
            label = gettext("7 d"),
            step = "day",
            stepmode = "backward"
          ),
          list(
            count = 1,
            label = gettext("1 m"),
            step = "month",
            stepmode = "backward"
          ),
          list(
            count = 3,
            label = gettext("3 m"),
            step = "month",
            stepmode = "backward"
          ),
          list(
            step = "all",
            label = gettext("all")
          )
        ),
        rangeslider = list(type = "date")
      )
    ),
    autosize = TRUE,
    yaxis = list(title = gettext("Round-trip time [ms]"))
  )
fig <- fig |>
  config(displayModeBar = TRUE, locale = locale_lang_code()) |>
  htmlwidgets::onRender(add_csv_button_local())
fig
