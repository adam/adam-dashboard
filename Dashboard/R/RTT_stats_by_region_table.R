df_rtt <- fromJSONwithToken(api_endpoint("/cz_rtt_cc_server_ipv_1d",
  ts = get_ts(days, 30)
))
df_qps <- fromJSONwithToken(api_endpoint("/cz_qps_cc_server_ipv_1d",
  ts = get_ts(days, 30)
))

df_rtt_qps <- left_join(df_qps, df_rtt) %>%
  mutate(
    country_code = ifelse(is.na(country_code), "NA", country_code),
    ts = as_datetime(ts, tz = "Europe/Prague"),
    region = countrycode::countrycode(country_code, "iso2c", "region23",
      custom_match = c(
        XK = "Eastern Europe", TF = "Melanesia",
        IO = "Southern Asia",
        GS = "South America", CX = "Australia and New Zealand",
        CC = "South-Eastern Asia", BV = "Southern Africa",
        AQ = "Antarctica", HM = "South-Eastern Asia"
      )
    ),
    continent = countrycode::countrycode(country_code, "iso2c", "continent",
      custom_match = c(
        XK = "Europe", TF = "Oceania",
        IO = "Asia",
        GS = "Americas", CX = "Oceania",
        CC = "Asia", BV = "Africa",
        AQ = "Antarctica", HM = "Asia"
      )
    ),
    region = ifelse(region %in%
        c("Melanesia", "Micronesia", "Polynesia"),
    gettext("Pacific Islands"),
    ifelse(region %in%
      c(
        "Eastern Africa", "Middle Africa",
        "Southern Africa"
      ),
    gettext("South-East Africa"),
    ifelse(region %in%
      c(
        "Eastern Asia", "South-Eastern Asia",
        "Southern Asia"
      ),
    gettext("South-Eastern Asia"),
    ifelse(region %in%
          c("Western Asia", "Central Asia"),
    gettext("Western-Central Asia"),
    ifelse(region %in%
      c("Central America", "Caribbean"),
    gettext("Central America & Caribbean"),
    ifelse(region %in%
      c(
        "Western Africa",
        "Northern Africa"
      ),
    gettext("North-West Africa"),
    region
    )
        )
      )
    )
    )
    )
  ) %>%
  mutate(region = ifelse(region == "South America", gettext("South America"),
    ifelse(region == "Southern Europe", gettext("Southern Europe "),
      ifelse(region == "Northern Europe", gettext("Northern Europe"),
        ifelse(region == "Eastern Europe", gettext("Eastern Europe"),
          ifelse(region == "Western Europe", gettext("Western Europe"),
            region
          )
        )
      )
    )
  )) %>%
  filter(!is.na(mean_median_rtt))



df_rtt_qps %>%
  filter(ts >= max(ts) - 30) %>%
  group_by(region) %>%
  summarise(
    min_rtt = quantile(mean_median_rtt, 0.10, na.rm = TRUE),
    first_q_rtt = quantile(mean_median_rtt, 0.25, na.rm = TRUE),
    mean_rtt = mean(mean_median_rtt, na.rm = TRUE),
    median_rtt = median(mean_median_rtt, na.rm = TRUE),
    wgt_mean_rtt = weighted.mean(mean_median_rtt, qps,
      na.rm = TRUE
    ),
    third_q_rtt = quantile(mean_median_rtt, 0.75, na.rm = TRUE),
    max_rtt = quantile(mean_median_rtt, 0.90, na.rm = TRUE),
    stdv_rtt = sd(mean_median_rtt, na.rm = TRUE)
  ) %>%
  filter(!is.na(region)) %>%
  mutate(
    min_tile = adam_color_tile(
      min_rtt,
      "white", dashboard_colors[1]
    ),
    mean_tile = adam_color_tile(
      wgt_mean_rtt,
      "white", dashboard_colors[3]
    ),
    max_tile = adam_color_tile(
      max_rtt,
      "white", dashboard_colors[1]
    ),
    stdv_tile = adam_color_tile(
      stdv_rtt,
      "white", dashboard_colors[2]
    )
  ) %>%
  arrange(desc(wgt_mean_rtt)) %>%
  select(
    region,
    max_tile,
    mean_tile,
    min_tile,
    stdv_tile
  ) %>%
  kable(
    "html",
    table.attr = "class='dtable'",
    escape = FALSE,
    caption = details(
      sprintf(
        gettext("Statistics for the last 30 days before %s"),
        format(Sys.Date(), locale_date_format())
      ),
      paste0(
        "<p>",
        gettext("Meaning of symbols:"),
        "</p><dl><dt>D<sub>1</sub></b></dt><dd>",
        gettext("first decile (10% values are smaller)"),
        "</dd><dt>D<sub>9</sub></dt><dd>",
        gettext("ninth decile (10% values are higher)"),
        "</dd><dt>σ</dt><dd>",
        gettext("standard error"),
        "</dd></dl>"
      )
    ),
    align = c("r", "c", "c", "c", "c"),
    format.args = list(big.mark = " "),
    col.names = c(gettext("Region"), "D~9~", gettext("Mean"), "D~1~", "σ")
  ) %>%
  kable_styling(
    font_size = 13,
    bootstrap_options = c("striped", "hover", "condensed", "responsive"),
    full_width = FALSE,
    position = "float_right"
  )
