fredlog_requests <- fromJSONwithToken(
  api_endpoint("/fredlog_registrar_requests_by_result_1d",
    ts = get_ts(months, 1)
  ),
)


contact_requests <- fredlog_requests %>%
  mutate(ts = as.Date(ts)) %>%
  group_by(ts, request_type) %>%
  summarise(requests = sum(requests)) %>%
  ungroup() %>%
  filter(
    stringr::str_starts(request_type, "Contact")
  ) %>%
  mutate(request_type = stringr::str_remove_all(request_type, "Contact")) %>%
  spread(request_type, requests) %>%
  replace(is.na(.), 0)

details(
  sprintf(
    gettext("Registry operations related to contacts in the time period before %s"), # nolint long string
    format(Sys.Date(), locale_date_format())
  ),
  paste0(
    "<dl>",
    "<dt>Check</dt>",
    "<dd>",
    gettext("Check availability of a contact"),
    "</dd>",
    "<dt>Create</dt>",
    "<dd>",
    gettext("Create a contact"),
    "</dd>",
    "<dt>Delete</dt>",
    "<dd>",
    gettext("Delete a contact"),
    "</dd>",
    "<dt>Info</dt>",
    "<dd>",
    gettext("Get info about a contact"),
    "</dd>",
    "<dt>SendAuthInfo</dt>",
    "<dd>",
    gettext("Send authorization info associated with a contact"),
    "</dd>",
    "<dt>Transfer</dt>",
    "<dd>",
    gettext("Transfer a contact"),
    "</dd>",
    "<dt>Update</dt>",
    "<dd>",
    gettext("Update a contact"),
    "</dd>",
    "</dl>"
  )
)


contact_requests %>%
  plot_ly(connectgaps = FALSE) %>%
  add_trace(
    x = ~ts,
    y = ~Info,
    name = "Info",
    hovertemplate = ~ paste0(
      ts, "<BR>", gettext("Requests"), "<B>: ",
      formatC(round(Info, 2), format = "d", big.mark = " "), "</B>"
    ),
    type = "scatter",
    mode = "lines+markers",
    showlegend = TRUE,
    line = list(color = dashboard_pal(7)[1]),
    marker = list(color = dashboard_pal(7)[1], size = 8, symbol = "circle-dot")
  ) %>%
  add_trace(
    x = ~ts,
    y = ~Check,
    name = "Check",
    hovertemplate = ~ paste0(
      ts, "<BR>", gettext("Requests"), "<B>: ",
      formatC(round(Check, 2), format = "d", big.mark = " "), "</B>"
    ),
    type = "scatter",
    mode = "lines+markers",
    showlegend = TRUE,
    line = list(color = dashboard_pal(7)[2], dash = "dash"),
    marker = list(
      color = dashboard_pal(7)[2],
      size = 8, symbol = "cross-dot"
    )
  ) %>%
  add_trace(
    x = ~ts,
    y = ~Create,
    name = "Create",
    hovertemplate = ~ paste0(
      ts, "<BR>", gettext("Requests"), "<B>: ",
      formatC(round(Create, 2), format = "d", big.mark = " "), "</B>"
    ),
    type = "scatter",
    mode = "lines+markers",
    showlegend = TRUE,
    line = list(color = dashboard_pal(7)[4], dash = "dash"),
    marker = list(
      color = dashboard_pal(7)[4],
      size = 8, symbol = "triangle-up-dot"
    )
  ) %>%
  add_trace(
    x = ~ts,
    y = ~Update,
    name = "Update",
    hovertemplate = ~ paste0(
      ts, "<BR>", gettext("Requests"), "<B>: ",
      formatC(round(Update, 2), format = "d", big.mark = " "), "</B>"
    ),
    type = "scatter",
    mode = "lines+markers",
    showlegend = TRUE,
    line = list(color = dashboard_pal(7)[7], dash = "dot"),
    marker = list(
      color = dashboard_pal(7)[7],
      size = 8, symbol = "x"
    )
  ) %>%
  add_trace(
    x = ~ts,
    y = ~SendAuthInfo,
    name = "SendAuthInfo",
    hovertemplate = ~ paste0(
      ts, "<BR>", gettext("Requests"), "<B>: ",
      formatC(round(SendAuthInfo, 2), format = "d", big.mark = " "), "</B>"
    ),
    type = "scatter",
    mode = "lines+markers",
    showlegend = TRUE,
    line = list(color = dashboard_pal(7)[5]),
    marker = list(
      color = dashboard_pal(7)[5],
      size = 8, symbol = "hexagon"
    )
  ) %>%
  add_trace(
    x = ~ts,
    y = ~Transfer,
    name = "Transfer",
    hovertemplate = ~ paste0(
      ts, "<BR>", gettext("Requests"), "<B>: ",
      formatC(round(Transfer, 2), format = "d", big.mark = " "), "</B>"
    ),
    type = "scatter",
    mode = "lines+markers",
    showlegend = TRUE,
    line = list(color = dashboard_pal(7)[6], dash = "dash"),
    marker = list(
      color = dashboard_pal(7)[6],
      size = 8, symbol = "star-diamond-dot"
    )
  ) %>%
  add_trace(
    x = ~ts,
    y = ~Delete,
    name = "Delete",
    hovertemplate = ~ paste0(
      ts, "<BR>", gettext("Requests"), "<B>: ",
      formatC(round(Delete, 2), format = "d", big.mark = " "), "</B>"
    ),
    type = "scatter",
    mode = "lines+markers",
    showlegend = TRUE,
    line = list(color = dashboard_pal(7)[3]),
    marker = list(
      color = dashboard_pal(7)[3],
      size = 8, symbol = "square-cross"
    )
  ) %>%
  config(displayModeBar = TRUE, locale = locale_lang_code()) %>%
  layout(
    separators = ".\u202F",
    autosize = TRUE,
    xaxis = list(
      title = gettext("Date"),
      range = as.POSIXct(c(
        min(contact_requests$ts),
        max(contact_requests$ts)
      )),
      rangeselector = list(
        buttons = list(
          list(
            count = 7,
            label = gettext("7 d"),
            step = "day",
            stepmode = "backward"
          ),
          list(
            count = 15,
            label = gettext("15 d"),
            step = "day",
            stepmode = "backward"
          ),
          list(
            count = 21,
            label = gettext("21 d"),
            step = "day",
            stepmode = "backward"
          ),
          list(
            count = n_distinct(contact_requests$ts),
            step = "day",
            label = gettext("1 m")
          )
        ),
        rangeslider = list(type = "date")
      )
    ),
    yaxis = list(
      title = gettext("Contact operations")
    )
  ) %>%
  onRender(add_csv_button_local())
