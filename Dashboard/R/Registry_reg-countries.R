domains_cc_latest <- fromJSON(api_endpoint(
  "/fred_domains_by_cc_latest",
  zone = "eq.cz"
))

domains_cc <- fromJSON(api_endpoint("/fred_domains_by_cc",
  zone = "eq.cz",
  ts = get_ts(days, 30)
))

domains_cc_wc <- fromJSONwithToken(api_endpoint("/fred_wc_domains_by_cc"))


country_codes <- load_country_codes() %>%
  rename(cc = country_code)

df_domains_country <- domains_cc_latest %>%
  inner_join(country_codes, by = c("cc")) %>%
  mutate(
    ts = as.Date(ts), Country = as.factor(Country),
    Country = fct_lump_n(Country,
      n = 14, w = domains,
      other_level = gettext("Other")
    ),
    cc = fct_lump_n(cc,
      n = 14, w = domains,
      other_level = gettext("Other")
    )
  ) %>%
  group_by(ts, Country, cc) %>%
  summarise(count = round(sum(domains), 0)) %>%
  ungroup() %>%
  select(-ts)

df_domains_country_no_cz <- df_domains_country %>%
  filter(cc != "CZ")

df_domains_country_cz <- df_domains_country %>%
  filter(cc == "CZ")

domains_country <- domains_cc %>% inner_join(country_codes, by = c("cc"))

top_cc <- unique(levels(df_domains_country$Country))

df_domains_cc_traffic <- domains_country %>%
  mutate(ts = as.Date(ts)) %>%
  group_by(ts, Country) %>%
  summarise(domains = sum(domains))

df_domains_country_wc <- domains_cc_wc |>
  mutate(ts = as.Date(ts)) |>
  filter(ts == max(ts)) |>
  pivot_wider(
    names_from = predicted,
    values_from = domains,
    values_fill = 0
  ) |>
  mutate(
    parked_perc =
      paste0(
        round(parked * 100 / (http_error + no_content + normal + parked), 1),
        " %"
      )
  ) |>
  left_join(country_codes, by = "cc") |>
  select(Country, cc, parked_perc)

df_domains_country_wc_other <- domains_cc_wc |>
  mutate(ts = as.Date(ts)) |>
  left_join(country_codes, by = "cc") |>
  filter(ts == max(ts)) |>
  filter(!(Country %in% top_cc)) |>
  pivot_wider(
    names_from = predicted,
    values_from = domains,
    values_fill = 0
  )

other_parked <-
  paste0(
    round(
      sum(df_domains_country_wc_other$parked) * 100 /
        (sum(df_domains_country_wc_other$http_error) +
           sum(df_domains_country_wc_other$no_content) +
           sum(df_domains_country_wc_other$normal) +
           sum(df_domains_country_wc_other$parked)),
      1
    ),
    " %"
  )

df_domains_cc_sparkline <-
  df_domains_cc_traffic %>%
  filter(Country %in% top_cc) %>%
  group_by(Country) %>%
  arrange(ts) %>%
  summarise(
    time_bar = spk_chr(
      domains,
      type = "line",
      lineColor = sparkline_colors[1],
      fillColor = sparkline_colors[2],
      spotColor = TRUE,
      width = 80
    )
  ) %>%
  rbind(df_domains_cc_traffic %>%
    filter(!(Country %in% top_cc)) %>%
    mutate(Country = gettext("Other")) %>%
    group_by(ts, Country) %>%
    summarise(domains = sum(domains)) %>%
    group_by(Country) %>%
    arrange(ts) %>%
    summarise(
      time_bar = spk_chr(
        domains,
        type = "line",
        lineColor = sparkline_colors[1],
        fillColor = sparkline_colors[2],
        spotColor = TRUE,
        width = 80
      )
    ))


htmltools::attachDependencies(
  htmltools::div(), html_dependency_sparkline()
)

options(knitr.kable.NA = "")

df_output <- df_domains_country_no_cz %>%
  bind_rows(df_domains_country_cz) %>%
  arrange(desc(count)) %>%
  left_join(df_domains_country_wc, by = c("Country")) %>%
  select(Country, count, parked_perc) %>%
  mutate(parked_perc = replace_na(parked_perc, other_parked)) %>%
  left_join(df_domains_cc_sparkline, by = c("Country")) %>%
  mutate(
    Country = as.character(Country),
    Country = ifelse(Country == gettext("United Kingdom"),
      gettext("UK"),
      ifelse(Country == gettext("United States"),
        gettext("USA"), Country
      )
    )
  )

rbind(
  df_output %>% filter(Country != gettext("Other")),
  df_output %>% filter(Country == gettext("Other"))
) %>%
  kable(
    "html",
    format.args = list(big.mark = "&#8239;", decimal.mark = ","),
    col.names = c(
      gettext("Country"), gettext("Domains"),
      gettext("Parked"), "Trend"
    ),
    escape = FALSE,
    align = c("c", "r", "r", "l")
  ) %>%
  column_spec(1, width = "5cm") %>%
  column_spec(2, width = "5cm") %>%
  column_spec(3, width = "5cm") %>%
  column_spec(4, width = "6cm") %>%
  kable_styling(
    bootstrap_options = c("striped", "hover", "condensed", "responsive"),
    full_width = FALSE,
    position = "float_left"
  )
