whois_rdap <- fromJSONwithToken(
  api_endpoint("/fredlog_public_services_requests_by_result_and_type_1d",
    ts = get_ts(days, 7)
  )
)

whois <- whois_rdap %>%
  mutate(ts = as.Date(ts)) %>%
  mutate(
    service = str_remove_all(service, "_whois"),
    result_code = ifelse(result_code == "NoEntriesFound", "Not Found",
      ifelse(result_code == "NotFound", "Not Found",
        ifelse(result_code == "BadRequest", "Bad Request",
          ifelse(result_code == "InternalServerError", "Error",
            ifelse(result_code == "Error", "Error", "Ok")
          )
        )
      )
    )
  ) %>%
  filter(result_code %in% c("Not Found", "Ok")) %>%
  group_by(ts, service, result_code) %>%
  summarise(requests = sum(requests)) %>%
  mutate(requests_bars = requests) %>%
  ungroup()


whois_bar <- whois %>%
  ggplot(aes(
    x = ts, y = requests_bars, fill = result_code,
    text = paste0(
      "<b>", gettext("Result Code"), "</b>: ", result_code,
      "<br><b>", gettext("Requests"), "</b>: ", requests
    )
  )) +
  geom_bar(stat = "identity") +
  facet_wrap(~service, scale = "free_y") +
  theme_minimal() +
  labs(x = gettext("Date"), y = gettext("Requests"),
       fill = gettext("Result Code")) +
  scale_fill_manual(values = rev(dashboard_pal(2))) +
  scale_x_date(date_labels = "%d.%m") +
  scale_y_continuous(labels = scales::format_format(big.mark = " ",
                                                    decimal.mark = ",",
                                                    scientific = FALSE)) +
  theme(panel.spacing = unit(-1.5, "lines"))

details(
  sprintf(
    gettext("Whois requests by user interface in the week before %s"),
    format(Sys.Date(), locale_date_format())
  ),
  paste0(
    "<dl>",
    "<dt>rdap</dt>",
    "<dd>",
    gettext("RDAP protocol (successor to Whois)"),
    "</dd>",
    "<dt>unix</dt>",
    "<dd>",
    gettext("traditional Whois command-line interface"),
    "</dd>",
    "<dt>web</dt>",
    "<dd>",
    gettext("web interface"),
    "</dd>",
    "</dl>"
  )
)

ggplotly(whois_bar, tooltip = "text") %>%
  config(displayModeBar = TRUE, locale = locale_lang_code()) %>%
  htmlwidgets::onRender(add_csv_button_local()) %>%
  layout(
    hovermode = "x unified",
    margin = list(t = 50),
    xaxis = list(fixedrange = TRUE)
  ) %>%
  layout(yaxis = list(fixedrange = TRUE))
