qps_total_1h <- fromJSON(api_endpoint("/cz_qps_total_1h",
  ts = get_ts(days, 30)
))
qps_odvr <- fromJSON(api_endpoint("/odvr_qps_total_1h",
  ts = get_ts(days, 30)
))
qps_day <- qps_total_1h %>%
  mutate(ts = as.Date(ts)) %>%
  group_by(ts) %>%
  summarise(qps = mean(qps)) %>%
  ungroup() %>%
  inner_join(
    qps_odvr %>%
      mutate(ts = as.Date(ts)) %>%
      group_by(ts) %>%
      summarise(qps_odvr = mean(qps)) %>%
      ungroup(),
    by = c("ts")
  )

plot_ly(
  qps_day,
  x = ~ts,
  y = ~qps,
  name = "DNS",
  type = "scatter",
  mode = "lines",
  line = list(color = dashboard_colors[1]),
  hovertemplate = paste0(
    "<b>", gettext("Date"), "</b>: %{x}",
    "<br><b>qps</b>: %{y}"
  )
) %>%
  add_trace(
    y = ~qps_odvr,
    name = "ODVR",
    mode = "lines",
    line = list(color = dashboard_colors[3]),
    hovertemplate = paste0(
      "<b>", gettext("Date"), "</b>: %{x}",
      "<br><b>qps</b>: %{y}"
    )
  ) %>%
  config(displayModeBar = FALSE, locale = locale_lang_code()) %>%
  layout(
    xaxis = list(title = gettext("Date")),
    yaxis = list(title = gettext("Queries per second"))
  )
