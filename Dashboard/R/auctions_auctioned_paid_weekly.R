df_amounts_week <- fromJSONwithToken(api_endpoint("/auctions_auctions_summary")) |>
  select(date, tot_sum, paid) |>
  mutate(
    ts = as.Date(date),
    paid = replace_na(paid, 0),
    tot_sum = replace_na(tot_sum, 0)
  )


df_amounts_week <- df_amounts_week |>
  mutate(
    week = week(date),
    year = year(date)
  ) |>
  group_by(year, week) |>
  mutate(
    week_start = min(date),
    week_end   = max(date)
  ) |>
  select(
    ts,
    week_start,
    week_end,
    tot_sum,
    paid
  ) |>
  reframe(
    tot_sum = sum(tot_sum),
    paid = sum(paid),
    perc = round(paid * 100 / tot_sum, 2),
    week_start = week_start,
    week_end = week_end
  ) |>
  ungroup() |>
  arrange(week_end)


week_plot <- plot_ly(
  df_amounts_week,
  x = ~ as.Date(week_end),
  type = "scatter",
  mode = "lines",
  y = ~tot_sum,
  name = gettext("Auctioned sum"),
  line = list(color = dashboard_colors[1]),
  hovertemplate = paste0(
    "<b>", gettext("Auctioned sum"), "</b>: %{y} ",
    gettext("CZK"), "<extra></extra>"
  )
) |>
  add_trace(
    y = ~paid,
    name = gettext("Paid sum"),
    mode = "lines",
    line = list(color = dashboard_colors[3]),
    hovertemplate = paste0(
      "<b>", gettext("Paid sum"), "</b>: %{y} ",
      gettext("CZK"), "<extra></extra>"
    )
  ) |>
  add_trace(
    y = 0,
    name = gettext("Date"),
    line = list(color = "rgba(255, 0, 0, 0)"),
    customdata = paste0(
      gettext("from"),
      " <b>",
      format(df_amounts_week$week_start),
      "</b> ",
      gettext("to"),
      " <b>",
      format(df_amounts_week$week_end),
      "</b>"
    ),
    hovertemplate = paste0(
      gettext("Week"), " %{customdata}", "<extra></extra>"
    ),
    visible = TRUE,
    legendrank = 1,
    showlegend = FALSE
  ) |>
  layout(
    hovermode = "x unified",
    margin = list(t = 75),
    title = gettext("Weekly auctioned and paid sum"),
    legend = list(
      orientation = "h",
      xanchor = "center",
      x = 0.5,
      y = 1.08
    ),
    separators = ".\u202F",
    xaxis = list(
      title = gettext("Week"),
      rangeselector = list(
        buttons = list(
          list(
            count = 15,
            label = gettext("15 d"),
            step = "day",
            stepmode = "backward"
          ),
          list(
            count = 1,
            label = gettext("1 m"),
            step = "month",
            stepmode = "backward"
          ),
          list(
            count = 6,
            label = gettext("6 m"),
            step = "month",
            stepmode = "backward"
          ),
          list(
            count = 1,
            label = gettext("1 y"),
            step = "year",
            stepmode = "backward"
          ),
          list(
            step = "all",
            label = gettext("all")
          )
        ),
        rangeslider = list(type = "date")
      )
    ),
    yaxis = list(
      title = gettext("Sum (CZK)"),
      tickformat = ",d"
    )
  )

ratio <- plot_ly(df_amounts_week,
  x = ~ as.Date(week_end),
  y = ~perc,
  type = "scatter",
  name = paste0(
    gettext("Ratio of paid sum"), " (%)"
  ),
  mode = "lines",
  line = list(color = dashboard_colors[2]),
  hovertemplate = paste0(
    "<b>",
    gettext("Auctioned sum"), "</b>: ",
    formatC(df_amounts_week$tot_sum,
      big.mark = " "
    ),
    " ", gettext("CZK"), "<br><b>",
    gettext("Paid sum"), "</b>: ",
    formatC(df_amounts_week$paid,
      big.mark = " "
    ), " ", gettext("CZK"), "<br><b>",
    gettext("Ratio (%)"), "</b>: %{y}<br><b><extra></extra>"
  )
) |>
  add_trace(
    y = 0,
    name = gettext("Date"),
    line = list(color = "rgba(255, 0, 0, 0)"),
    customdata = paste0(
      gettext("from"),
      " <b>",
      format(df_sk1_week$week_start),
      "</b> ",
      gettext("to"),
      " <b>",
      format(df_sk1_week$week_end),
      "</b>"
    ),
    hovertemplate = paste0(
      gettext("Week"), " %{customdata}", "<extra></extra>"
    ),
    visible = TRUE,
    legendrank = 1,
    showlegend = FALSE
  ) %>%
  layout(
    xaxis = list(
      title = gettext("Date"),
      rangeselector = list(
        buttons = list(
          list(
            count = 15,
            label = gettext("15 d"),
            step = "day",
            stepmode = "backward"
          ),
          list(
            count = 1,
            label = gettext("1 m"),
            step = "month",
            stepmode = "backward"
          ),
          list(
            count = 6,
            label = gettext("6 m"),
            step = "month",
            stepmode = "backward"
          ),
          list(
            count = 1,
            label = gettext("1 y"),
            step = "year",
            stepmode = "backward"
          ),
          list(
            step = "all",
            label = gettext("all")
          )
        ),
        rangeslider = list(type = "date"),
        x = 0.8
      )
    ),
    yaxis = list(
      title = paste0(
        gettext("Ratio of paid sum"), " (%)"
      ),
      ticksuffix = "%"
    )
  )

subplot(week_plot, ratio,
  margin = 0.06, titleX = TRUE, titleY = TRUE, nrows = 1
) %>%
  layout(
    hovermode = "x unified",
    autosize = TRUE,
    xaxis = list(showgrid = TRUE, zeroline = FALSE),
    yaxis = list(showgrid = TRUE, zeroline = FALSE)
  ) %>%
  config(displayModeBar = TRUE, locale = locale_lang_code()) |>
  htmlwidgets::onRender(add_csv_button_local())
