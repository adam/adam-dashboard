df_blacklist <-
  fromJSONwithToken(api_endpoint("/auctions_blacklist")) |>
  mutate(date = as.Date(date)) |>
  arrange(date)

plot_ly(
  data = df_blacklist,
  name = "Blacklisted bidders",
  type = "scatter",
  mode = "line",
  x = ~date,
  y = ~total,
  hovertemplate = ~ paste0(
    "<b>", gettext("Number of bidders"),
    "</b>: ", total, "<extra></extra>"
  ),
  fillcolor = dashboard_colors[1]
) |>
  layout(
    hovermode = "x unified",
    xaxis = list(
      title = gettext("Date"),
      rangeselector = list(
        buttons = list(
          list(
            count = 15,
            label = gettext("15 d"),
            step = "day",
            stepmode = "backward"
          ),
          list(
            count = 1,
            label = gettext("1 m"),
            step = "month",
            stepmode = "backward"
          ),
          list(
            count = 6,
            label = gettext("6 m"),
            step = "month",
            stepmode = "backward"
          ),
          list(
            count = 1,
            label = gettext("1 y"),
            step = "year",
            stepmode = "backward"
          ),
          list(
            step = "all",
            label = gettext("all")
          )
        ),
        rangeslider = list(type = "date")
      )
    ),
    yaxis = list(
      title = gettext("Number of bidders")
    )
  ) |>
  config(displayModeBar = TRUE, locale = locale_lang_code()) |>
  htmlwidgets::onRender(add_csv_button("/auctions_blacklist"))
