fig <- fromJSONwithToken(api_endpoint("/auctions_winners")) |>
  filter(date == max(date)) |>
  select(date, account,total_amount, total_amount_paid, total_amount_unpaid) |>
  mutate(account = fct_lump_n(account,
    n = 10, w = total_amount,
    other_level = gettext("other")),
    short = paste0(substr(account, 1, 14), "....")) |>
  gather(type, count, total_amount, total_amount_paid, total_amount_unpaid) |>
  ggplot(aes(short, count,
    fill = type,
    text = paste0(
      date,
      "<br><b>", type,
      "<br><b>", account,
      "<br>", gettext("Sum (CZK)"), "</b>: ", formatC(count,
        format = "d", big.mark = " "
      )
    )
  )) +
  geom_bar(stat = "identity", fill = dashboard_pal(1)) +
  coord_flip() +
  facet_grid(. ~ type, scales = "free") +
  theme_bw() +
  labs(y = gettext("Sum (CZK)"), x = gettext("Anonymous accounts"))


ggplotly(fig, tooltip = "text") %>%
  config(displayModeBar = TRUE, locale = locale_lang_code()) %>%
  htmlwidgets::onRender(add_csv_button_local()) %>%
  layout(
    margin = list(t = 50),
    xaxis = list(fixedrange = TRUE)
  ) %>%
  layout(yaxis = list(fixedrange = TRUE)) 
