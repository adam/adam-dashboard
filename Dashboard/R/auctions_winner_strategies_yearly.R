df_winstrategy_year <-fromJSONwithToken(api_endpoint("/auctions_winner_bid_strategy_types"))|>
  mutate(
    ts = as.Date(date),
    year = year(ts), 
    bid_strategy_type = replace_na(bid_strategy_type, 0),
    count = replace_na(count, 0)
  ) |>
  spread(bid_strategy_type, count) |>
  group_by(year) %>%
  summarise(
    MAX = sum(MAX),
    SPOT = sum(SPOT)
  )

plot_ly(df_winstrategy_year,
        x = ~year,
        y = ~MAX,
        type = "bar",
        marker = list(color = dashboard_colors[1]),
        name = gettext("MAX"),
        hovertemplate = paste0(
          "<b>", gettext("MAX"), "</b>: ", df_winstrategy_year$MAX,
          "<extra></extra>"
        )
) |>
  add_trace(
    y = ~SPOT,
    name = gettext("SPOT"),
    marker = list(color = dashboard_colors[3]),
    hovertemplate = paste0(
      "<b>", gettext("SPOT"), "</b>: ",
      formatC(df_winstrategy_year$SPOT,
              format = "d",
              big.mark = "\u202F"
      ),"<extra></extra>"
    )
  ) %>%
  config(
    displayModeBar = TRUE,
    locale = locale_lang_code()
  ) %>%
  htmlwidgets::onRender(add_csv_button_local()) %>%
  layout(
    hovermode = "x unified",
    legend = list(
      orientation = "h",
      xanchor = "center",
      yanchor = "bottom",
      x = 0.5,
      y = 0.99
    ),
    separators = ".\u202F",
    margin = list(t = 50),
    xaxis = list(
      fixedrange = TRUE,
      dtick = 1,
      title = gettext("Year")
    )
  ) %>%
  layout(yaxis = list(
    fixedrange = TRUE,
    title = gettext("Count"),
    tickformat = ",d"
  ))
