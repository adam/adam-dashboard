ipv6_domains <-
  fromJSONwithToken(api_endpoint("/crawler_web_ipv", ts = get_ts(days, 30))) |>
  mutate(ts = as.Date(ts)) |>
  filter(domains !=0) |>
  filter( ts == max(ts)) |>
  group_by(ts) |>
  mutate(
    total = sum(domains),
    perc = (domains * 100) / total
  ) |>
  filter(ip_support != "ipv4 only") |>
  ungroup() |>
  summarize(
    ts   = unique(ts),
    perc = sum(perc)
  )

valueBox(
  paste0(
    prettyNum(sum(ipv6_domains$perc),
      big.mark = " ",
      digits   = 3
    ),
    "%"
  ),
  caption = details(
    gettext("IPv6 support on web servers"),
    paste0(
      gettext("Estimated on "),
      ipv6_domains$ts, ".",
      "<br>",
      gettext("Based on the latest DNS crawler scan, the percentage shows the ratio of all servers supporting IPv6.") # nolint long string
    )
  ),
  color = dashboard_colors[1]
)
