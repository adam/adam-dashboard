endpoint <- api_endpoint("/dk_tld_qps_cc_city_1h",
  ts = get_ts(days, 7)
)

df <- fromJSONwithToken(endpoint)

country_codes <- load_country_codes() %>%
  rename(
    client_country_code = "country_code",
    client_country = "Country"
  )

df <- df %>%
  left_join(country_codes, by = c("client_country_code")) %>%
  mutate(ts = as_datetime(ts))

plot_sankey_traffic(df, 15)
