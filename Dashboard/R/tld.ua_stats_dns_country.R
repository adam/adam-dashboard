qps_cc_1h <- fromJSONwithToken(api_endpoint("/ua_tld_qps_cc_1h",
                                            ts = get_ts(days, 30)))

country_codes <- load_country_codes()

qps_cc <- qps_cc_1h %>% inner_join(country_codes, by = c("country_code"))

df_qps_cc_traffic <- qps_cc %>%
  mutate(Country = as.factor(Country), ts = as.Date(ts),
    Country =
      fct_lump_n(
        Country,
        n = 15,
        w = qps,
        other_level = gettext("Other")
      )
  ) %>%
  group_by(ts, Country) %>%
  summarise(qps = sum(qps)) %>%
  mutate(queries = qps * 3600, queries_24h = queries / 86400)


df_qps_cc_traffic_table <- df_qps_cc_traffic %>%
  group_by(Country) %>%
  summarise(
    qps_max = round(max(queries_24h), 5),
    qps_min = round(min(queries_24h), 5)
  ) %>%
  ungroup() %>%
  mutate(
    qps_max_tile = adam_color_tile2(qps_max,
                                    "white",
                                    dashboard_colors[1],
                                    decimals = 5),
    qps_min_tile = adam_color_tile2(qps_min,
                                    "white",
                                    dashboard_colors[1],
                                    decimals = 5)
  ) %>%
  inner_join(
    df_qps_cc_traffic %>%
      group_by(Country) %>%
      summarise(
        qps_current = mean(queries_24h)
      ) %>%
      ungroup() %>%
      mutate(
        qps_current_tile = adam_color_tile2(qps_current,
                                            "white", dashboard_colors[3],
                                            decimals = 5)
      ),
    by = c("Country")
  )

df_qps_cc_traffic_spark <- df_qps_cc_traffic %>%
  arrange(ts) %>%
  group_by(Country) %>%
  summarise(
    queries_bar = spk_chr(
      queries_24h,
      type = "line",
      lineColor = sparkline_colors[1],
      fillColor = sparkline_colors[2],
      spotColor = TRUE,
      width = 80
    )
  )


htmltools::attachDependencies(
  htmltools::div(),
  html_dependency_sparkline()
)


df_qps_cc_traffic_table %>%
  left_join(df_qps_cc_traffic_spark, by = "Country") %>%
  mutate(
    Country = as.character(Country),
    Country = ifelse(is.na(Country), gettext("Unknown"),
                     ifelse(Country == gettext("United Kingdom"),
                            gettext("UK"),
                            ifelse(Country == gettext("United States"),
                                   gettext("USA"),
                                   Country)))
  ) %>%
  arrange(desc(qps_current)) %>%
  select(
    Country,
    qps_max_tile,
    qps_min_tile,
    queries_bar,
    qps_current_tile
  ) %>%
  kable(
    "html",
    table.attr = "class='dtable'",
    escape = FALSE,
    align = c("r", "c", "c", "c", "c"),
    format.args = list(big.mark = " "),
    col.names = c(
      gettext("Country"), "Max QPS", "Min QPS",
      "Trend", gettext("Average")
    )
  ) %>%
  column_spec(1, bold = TRUE, width = "3cm") %>%
  column_spec(2, width = "3cm") %>%
  column_spec(3, width = "3cm") %>%
  column_spec(4, width = "3cm") %>%
  column_spec(5, width = "3cm") %>%
  kable_styling(
    bootstrap_options = c("striped", "hover", "condensed", "responsive"),
    full_width = TRUE,
    position = "left"
  )
