qps_rcode_1h <-
  fromJSONwithToken(
    api_endpoint("/ua_tld_qps_rcode_1h",
      ts = get_ts(days, 30)
    )
  ) %>%
  mutate(rcode = as.factor(rcode))

rcode <- read.csv("../CSV/rcode.csv", sep = ",")

qps_rcode_plot2(qps_rcode_1h, rcode, 5)
