endpoint <- "/ua_qps_prot_1h"

qps_protocol_1h <- fromJSONwithToken(api_endpoint(endpoint,
  ts = get_ts(months, 6)
))

qps_protocol_plot(qps_protocol_1h, endpoint)
