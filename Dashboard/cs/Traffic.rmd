---
title: "Provoz"
output: 
  flexdashboard::flex_dashboard:
    orientation: rows
    vertical_layout: scroll
    css: ../adam.css
    includes: 
      in_header: "../header.html"
---

```{r setup, code=xfun::read_utf8('../R/Traffic_setup.R'), include=FALSE}
```
    
Row
-------------------------------------

### Resolvery

**Analýza zemí** – denní průměry QPS pro dotazy pocházející od resolverů z dané země v průběhu posledních 30 dnů před `r format(Sys.Date(), locale_date_format())`

```{r dns-resolvers, code=xfun::read_utf8('../R/Traffic_dns-country.R'), echo=F, warning=F}
```

**Analýza typů dotazů** – denní průměry QPS pro dotazy s daným typem (QTYPE) v průběhu posledních 30 dnů před `r format(Sys.Date(), locale_date_format())`

```{r dns-qtype, code=xfun::read_utf8('../R/Traffic_qtype.R'), echo=F, warning=F}
```


### Toky DNS provozu – průměrné rozdělení QPS od `r format(Sys.Date()-7)`

```{r dns-traffic, code=xfun::read_utf8('../R/Traffic_dns-traffic.R'), echo=F, warning=F, fig.align="center"}
``` 

### Autoritativní servery

**Analýza serverů** – denní průměry QPS pro dotazy přijaté autoritativním serverem v daném městě v průběhu posledních 30 dnů před `r format(Sys.Date(), locale_date_format())`

```{r dns-servers, code=xfun::read_utf8('../R/Traffic-dns-servers.R'), echo=F, warning=F}
```

**Analýza kódů odpovědi** – denní průměry QPS pro dotazy, na něž byla odeslána odpověď s daným kódem (RCODE) v průběhu posledních 30 dnů před `r format(Sys.Date(), locale_date_format())`

```{r dns-rcode, code=xfun::read_utf8('../R/Traffic_rcode.R'), echo=F, warning=F}
```

Row {.tabset}
--------------------------

### Verze IP

**Intenzita provozu podle verze IP** – časová řada denních průměrů QPS pro IPv4 a IPv6

```{r ipv, code=xfun::read_utf8('../R/Traffic_ipv.R'), echo=F, warning=F}
```

### Protokol

**Intenzita provozu podle transportního protokolu** – časová řada denních průměrů QPS pro UDP a TCP

```{r protocol, code=xfun::read_utf8('../R/Traffic_protocol.R'), echo=F, warning=F}
```

### Země SLD

Umístění autoritativních DNS serverů pro domény druhé úrovně pod *.cz* – státy (k datu `r format(as.Date(fromJSON(api_endpoint("/crawler_ns_location_cc_latest", limit=1, order="ts.desc", select="ts"))$ts), locale_date_format())`)

```{r location_cc, code=xfun::read_utf8('../R/Traffic_location_cc.R'), echo=F}
```

### Sítě SLD

Umístění autoritativních DNS serverů pro domény druhé úrovně pod *.cz* – autonomní systémy (k datu `r format(as.Date(fromJSON(api_endpoint("/crawler_ns_location_as_latest", limit=1, order="ts.desc", select="ts"))$ts), locale_date_format())`)

```{r location, code=xfun::read_utf8('../R/Traffic_location_network_AS.R'), echo=F}
```

### Spádové oblasti

Přiřazení zemí k cílovému DNS serveru, který dostává většinu provozu z dané země.

```{r map, code=xfun::read_utf8('../R/Traffic_server_map.R'), echo=F}
```

### DNSSEC {data-hide-in-master=true}

```{r dnssec_qps, code=xfun::read_utf8('../R/DNSSEC_traffic.R'), echo=F, warning=F}
```
