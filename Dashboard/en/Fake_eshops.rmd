---
title: "Fake eshops"
output: 
  flexdashboard::flex_dashboard:
    orientation: rows
    vertical_layout: scroll
    css: ../adam.css
    includes: 
      in_header: "../header.html"
---


```{r setup, code=xfun::read_utf8('../R/Fake_eshops_setup.R'), include=FALSE}
```


Row
-------------------------------------

### Fake eshops

```{r eshop_detail, code=xfun::read_utf8('../R/Fake_eshops_details.R'), echo=F}
```

```{r fakeshop_table, code=xfun::read_utf8('../R/Fake_eshops.R'), echo=F, warning=F}
```

### Holders

Holders of fake e-shop domains detected in the last 6 months

```{r holder_table, code=xfun::read_utf8('../R/Fake_eshops_holderID_table.R'), echo=F, warning=F}
```



Row {.tabset}
-------------------------------------

### E-shop classification

E-shops classification on `r format(as.Date(fromJSONwithToken(api_endpoint("/crawler_fake_eshop_2w_export_all", limit=1, order="last_seen.desc", select="last_seen"))$last_seen), locale_date_format())`


```{r predic_fakeshop, code=xfun::read_utf8('../R/Fake_eshops_barplot.R'), echo=F, warning=F, fig.width=5}
```


### Goods

Relative frequencies of estimated goods categories offered by fake e-shops in the last month

```{r fakeshop_topics, code=xfun::read_utf8('../R/Fake_eshops_topics.R'), echo=F, warning=F}
```

### Registrars

Distribution of likely fake e-shops among registrars in the last 6 months

```{r fakeshop_registrars, code=xfun::read_utf8('../R/Fake_eshops_registrars.R'), echo=F, warning=F}
```

### Holder's country

Distribution of likely fake e-shops among holder's country in the last 6 months

```{r fakeshop_holdercc, code=xfun::read_utf8('../R/Fake_eshops_holder_cc.R'), echo=F, warning=F}
```
