---
title: "RTT"
output: 
  flexdashboard::flex_dashboard:
    orientation: rows
    vertical_layout: scroll
    css: ../adam.css
    includes: 
      in_header: "../header.html"
---

```{r setup, code=xfun::read_utf8('../R/Datart_RTT_setup.R'), include=FALSE}
```

**Round-trip time (RTT)** is estimated only for DNS transactions over TCP. Values displayed in the graphs and tables on this page are computed as *weighted means* for particular aggregation units (countries, regions, continents) where the corresponding QPS value is taken as the weight. The values may be influenced by incorrect results of GeoIP classification.

Row {data-height=600}
---------------------------------

### RTT worldmap

```{r rtt_map, code=xfun::read_utf8('../R/Datart_RTT_map.R'), echo=F, warning=F}
```

### Countries

```{r rtt_country_desc, code=xfun::read_utf8('../R/Datart_RTT_country_search_details.R'), echo=F, warning=F}
```

```{r rtt_country_search, code=xfun::read_utf8('../R/Datart_RTT_country_search.R'), echo=F, warning=F}
```

Row {.tabset data-height=500}
-----------------------------------------

### Continents

<div class="adam-flex-container">
```{r rtt_continent_table, code=xfun::read_utf8('../R/Datart_RTT_stats_by_continent_table.R'), echo=F, warning=F}
```

```{r rtt_continent, code=xfun::read_utf8('../R/Datart_RTT_continents.R'), echo=F, warning=F}
```
</div>

### Worlwide regions

<div class="adam-flex-container">
```{r rtt_region_table, code=xfun::read_utf8('../R/Datart_RTT_stats_by_region_table.R'), echo=F, warning=F}
```

<div>
```{r rtt_region_desc, code=xfun::read_utf8('../R/Datart_RTT_by_regions_details.R'), echo=F, warning=F}
```

```{r rtt_region, code=xfun::read_utf8('../R/Datart_RTT_by_regions.R'), echo=F, warning=F}
```
</div>
</div>
