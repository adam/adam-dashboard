---
title: "Summary"
output: 
  flexdashboard::flex_dashboard:
    orientation: rows
    vertical_layout: scroll
    css: ['../adam.css', '../Summary.css']
    includes: 
      in_header: "../header.html"
---

```{r setup, code=xfun::read_utf8('../R/Summary_setup.R'), include=FALSE}
```

Row 
-------------------------------------------------------

### Welcome to CZ.NIC Statistics

This web page is the entry point to graphs and statistics that are related to most aspects of CZ.NIC operations. Machine access to source data is possible via [REST API](https://stats.nic.cz/openapi.json), see the [OpenAPI UI](https://stats.nic.cz/swagger/). Under the name [Domain Report](https://adam.nic.cz/en/domain-reports/), CZ.NIC also publishes an annual data summary on the developments in the .CZ domain.

Row 
-------------------------------------------------------

###

```{r domain-count, code=xfun::read_utf8('../R/Summary_domain-count.R'), echo=F, warning=F}
```

###

```{r dnssec-domains, code=xfun::read_utf8('../R/Summary_dnssec-domains.R'), echo=F, warning=F}
```

###

```{r qpsday, code=xfun::read_utf8('../R/Summary_qpsday.R'), echo=F, warning=F}
```

###

```{r newdom, code=xfun::read_utf8('../R/Summary_newdom.R'), echo=F, warning=F}
```

Row {data-height=320}
----------------------------------------------------

### CZ Domain Registry

Some statistics are generated directly from the [CZ Domain Registry](https://www.nic.cz/page/744/registracni-system/) – the master database of all domain names registered in the CZ top-level domain with associated information about the domain owner, registrar etc.

The graph on the right shows the evolution of the number of registered domains since 2008, together with the number of DNSSEC-secured domains.

<a href="Registry.html" class="summary-button">Registry stats</a>

### Number of registered domains

```{r registered-domains, code=xfun::read_utf8('../R/Summary_registered-domains.R'), echo=F}
```

Row {data-height=320}
----------------------------------------------------

### DNS Traffic

CZ.NIC operates more than 100 authoritative servers for the CZ domain, located in ten countries worldwide, and four instances of [ODVR](https://www.nic.cz/odvr/) public resolvers. Records about all DNS transactions handled by each server (with anonymised sources for the resolvers) are collected in an Apache Hadoop database, and then used for generating many statistics and performing ad hoc analyses.

The graph on the left displays a time series of the average number of DNS queries per second processed by servers of each type during the last month.

<a href="Traffic.html" class="summary-button">Auth server stats</a>
<a href="ODVR.html" class="summary-button">ODVR stats</a>

### DNS server traffic

```{r dns-server-traffic, code=xfun::read_utf8('../R/Summary_dns-server-traffic.R'), echo=F}
```

Row {data-height=320}
------------------------------------------------------

### ![MojeID](../images/mojeid.svg){height=1em}

Since 2010, CZ.NIC operates the [MojeID](https://www.mojeid.cz/en/) public authentication service. In 2020, MojeID was accredited by Ministry of interior and connected to NIA (National Identity Authority) as authentication option for access to governmental services.

The graph on the right shows the number of all logins to MojeID as well as logins to NIA during the last month.

<a href="MojeID.html" class="summary-button">MojeID stats</a>

### MojeID and NIA logins

```{r summary_mojeid, code=xfun::read_utf8('../R/Summary_mojeID.R'),echo=F}
```
