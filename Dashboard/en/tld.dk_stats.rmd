---
title: ".dk"
output: 
  flexdashboard::flex_dashboard:
    orientation: rows
    vertical_layout: scroll
    css: ../adam.css
    includes: 
      in_header: "../header.html"
---


```{r setup, code=xfun::read_utf8('../R/tld.dk_stats_setup.R'), include=FALSE}
```


Row
-------------------------------

### Resolvers

**Country analysis** – daily QPS averages for queries originating from resolvers in a given country during the last 30 days before `r format(Sys.Date(), locale_date_format())`

```{r dns-country, code=xfun::read_utf8('../R/tld.dk_stats_dns_country.R'), echo=F, warning=F}
```

**Query type analysis** – daily QPS averages for queries with a given QTYPE during the last 30 days before `r format(Sys.Date(), locale_date_format())`

```{r dns-qtype, code=xfun::read_utf8('../R/tld.dk_stats_qtype.R'), echo=F, warning=F}
```

### Traffic flows – average distribution of QPS since `r format(Sys.Date()-7)`.


```{r dns-traffic, code=xfun::read_utf8('../R/tld.dk_stats_dns_traffic.R'), echo=F, warning=F}
```

### Authoritative servers

**Server analysis** – daily QPS averages for queries reaching an authoritative server in a given city during the last 30 days before `r format(Sys.Date(), locale_date_format())`

```{r dns-servers, code=xfun::read_utf8('../R/tld.dk_stats_dns_servers.R'), echo=F, warning=F}
```

**Response code analysis** – daily QPS averages for queries responded with a given RCODE during the last 30 days before `r format(Sys.Date(), locale_date_format())`

```{r dns-rcode, code=xfun::read_utf8('../R/tld.dk_stats_rcode.R'), echo=F, warning=F}
```

Row {.tabset}
-------------------------------


### DNS traffic

**DNS traffic** – time series of daily QPS averages
  
```{r qpstotal, code=xfun::read_utf8('../R/tld.dk_stats_dns_total.R'), echo=F, warning=F}
``` 

### IP version

**Traffic rate by IP version** – time series of daily QPS averages for IPv4 and IPv6

```{r ipv, code=xfun::read_utf8('../R/tld.dk_stats_ipv.R'), echo=F, warning=F}
```

### Protocol

**Traffic rate by transport protocol** – time series of daily QPS averages for UDP and TCP

```{r protocol, code=xfun::read_utf8('../R/tld.dk_stats_protocol.R'), echo=F, warning=F}
``` 
