FROM rocker/r-base:4.3.1

ARG TRINO_CONNECTOR_TOKEN=${TRINO_CONNECTOR_TOKEN:-""}
ENV TRINO_CONNECTOR_TOKEN=${TRINO_CONNECTOR_TOKEN}

RUN printf 'cs_CZ.UTF8 UTF-8\nen_US.UTF8 UTF-8\n' > /etc/locale.gen && locale-gen
RUN ln -sf /usr/bin/python3 /usr/bin/python
RUN apt-get update -yqq --allow-releaseinfo-change
RUN apt-get upgrade -yqq
RUN apt-get autoremove -yqq
# System dependencies:
RUN apt-get install -yqq \
    python3 \
    python3-pip \
    file \
    pandoc \
    libxml2-dev \
    libcurl4-openssl-dev \
    libudunits2-dev \
    libgdal-dev \
    libssl-dev \
    libgeos-dev \
    libproj-dev \
    gettext \
    libfontconfig-dev \ 
    libharfbuzz-dev \
    libfribidi-dev \
    libglpk-dev \
    libicu72 \
    jq \
    python3-yaml \
    curl \
    git \
    gfortran
RUN install2.r -e \
    flexdashboard \
    rmarkdown \
    sass \
    pkgdown \
    devtools \
    dplyr \
    leaflet \
    leaflet.extras \
    tmap \
    ggspatial \
    gridExtra \
    sp \
    ggplot2 \
    plotly \
    lubridate \
    htmltools \
    htmlwidgets \
    tidyr \
    knitr \
    kableExtra \
    formattable \
    sparkline \
    jsonlite \
    lazyeval \
    forcats \
    stringr\
    lintr \
    readODS \
    countrycode \
    networkD3 \
    colorspace \
    RColorBrewer \
    DT \
    reticulate \
    readr
RUN R -e 'reticulate::install_miniconda()'
RUN R -e 'reticulate::conda_install("r-reticulate", "python-kaleido")'
RUN R -e 'reticulate::conda_install("r-reticulate", "plotly", channel = "plotly")'
RUN R -e 'reticulate::use_miniconda("r-reticulate")'
RUN R -e 'install.packages("https://cran.r-project.org/src/contrib/Archive/maptools/maptools_1.1-8.tar.gz")'
RUN installGithub.r \
    htmlwidgets/sparkline \
    mattflor/chorddiag
RUN R -e 'devtools::install_git("https://oauth2:${TRINO_CONNECTOR_TOKEN}@gitlab.nic.cz/adam/CZNICLabsTrinoConnector.git")'
RUN python3 -m pip install 'requests==2.31.0' 'check-jsonschema==0.23.3' --break-system-packages
