# Adam Dashboard

## Branches and generated pages

* [master](https://gitlab.nic.cz/adam/adam-dashboard/-/tree/master): https://stats.nic.cz/dashboard/en/
* [devel](https://gitlab.nic.cz/adam/adam-dashboard/-/tree/devel): https://stats.nic.cz/dashboard-devel/en/
* [fake-eshops-page](https://gitlab.nic.cz/adam/adam-dashboard/-/tree/fake-eshops-page): https://stats.nic.cz/dashboard-fake-eshops-page/en/

## Other resources

* [Login](https://stats.nic.cz/accounts/login/)
* [Token administration](https://stats.nic.cz/tokens/)
* [Wiki](https://gitlab.nic.cz/adam/adam-dashboard/-/wikis/)
* [Localization of R strings](https://gitlab.nic.cz/adam/adam-dashboard/-/wikis/l10n)
