# Extracts the CI build script for local usage

# $ python build-local.py > build-local.sh
# $ export API_TOKEN="…"
# $ export TRINO_CONNECTOR_TOKEN="…"
# $ bash build-local.sh
# …
# $ python -m http.server --directory Dashboard

# Please don't commit the generated  `build-local.sh` script to Git

import yaml

y = yaml.safe_load(open(".gitlab-ci.yml", "r"))
print(
    "\n".join(
        [
            "\n".join(f"{v}=\"{str(y['variables'][v])}\"" for v in y["variables"].keys()),
            "\n".join(y["build"]["before_script"]),
            "\n".join(y["build"]["script"]),
        ]
    )
)
