#!/bin/bash

set -e;

if [ "$CHECK_CENTR_SCHEMA" != "0" ]; then
    curl "https://stats.centr.org/schemas/registration_data_v2.1.json" > "registration_data_v2.1.json"
    check-jsonschema --schemafile registration_data_v2.1.json public/centr_stats.json
    rm "registration_data_v2.1.json"
else
    echo "Schema check disabled by env variable"
fi