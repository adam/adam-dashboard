---
title: "Dokumentace ke statistickým stránkám"
output:
  html_document:
    toc: TRUE
    css: ../docs.css
---

Toto je dokumentace ke [statistickým stránkám](https://stats.nic.cz/dashboard/cs/Summary.html) vydávaným sdružením CZ.NIC.

# Interaktivní dashboardy

Statistiky CZ.NIC jsou zpracovány v podobě interaktivních dashboardů. Každý dashboard pokrývá specifický aspekt činnosti sdružení CZ.NIC, a lze se na něj dostat přes jeho jméno v horní liště stránky [Přehled](https://stats.nic.cz/dashboard/cs/Summary.html).

Na dashboardech najdeme v různých uspořádáních kombinace grafů, tabulek a textu. Některé plochy v dashboardech jsou sdíleny pro více témat, mezi kterými je možno přepínat pomocí pojmenovaných záložek (tabů):

![](images/tabs.png)

S většinou grafů se dá různými způsoby interagovat v závislosti na typu konkrétního grafu. Grafy v této dokumentaci jsou rovněž aktivní, takže je možné všechny jejich vlastnosti vyzkoušet přímo zde.

Některé grafy nabízejí poblíž svého pravého horního rohu **lištu s nástroji**. Ta je viditelná jen tehdy, když na graf najedeme myší. Vypadá například takto:

![](../en/images/toolbar.png)

Ikony reprezentují různé možnosti *manipulace* s grafem anebo *stažení* grafu jako PNG obrázku či zdrojových dat ve formátu CSV. Po najetí myší na některou z ikon se zobrazí stručný popis funkce příslušného nástroje.

## Časové řady

Graf časové řady slouží ke znázornění časové posloupnosti datových bodů. Na vodorovné ose takového grafu bývá obvykle vynášen čas a na svislé ose pak hodnoty pozorované veličiny.

Tady je příklad:

```{r setup-odvr, code=xfun::read_utf8('../../Dashboard/R/ODVR_setup.R'), include=FALSE}
```
```{r odvr-protocol, code=xfun::read_utf8('../../Dashboard/R/ODVR_odvr-protocol.R'), echo=F, warning=F, message = FALSE}
```

Tento graf zobrazuje čtyři proměnné. Po najetí myší na datový bod kterékoli z čar se zobrazí přesné hodnoty obou souřadnic.

**Legenda** vpravo nahoře neslouží pouze k přiřazení barev čar proměnným, ale umožňuje také jednotlivé proměnné vypínat a zapínat kliknutím na příslušnou položku legendy. Dvojitým kliknutím na položku se dá dosáhnout toho, že se zobrazí pouze tato položka.

Po kliknutí na položku legendy se také vždy upraví škála na svislé ose podle aktuálně vybraných proměnných. Tak třeba v předchozím grafu po podrobnějším zkoumání zjistíme, že čára *TCP/53* v podstatě splývá s vodorovnou osou. Dvěma kliknutími na položky *UDP* a *TCP DoT* ale můžeme potlačit obě dominantní časové řady, a hned vidíme, že hodnoty *TCP/53* jsou sice malé, ale nenulové.

Tlačítka s časovým **rozsahem** umožňují zvolit délku zobrazeného časového intervalu, který je obvykle (jako i v tomto případě) ukotven napravo v posledním bodu časové řady.

## Sloupcový graf

[Sloupcový graf](https://cs.wikipedia.org/wiki/Sloupcov%C3%BD_graf) se hodí pro vizualizaci vztahu mezi numerickou a kategoriální proměnnou. Každé z diskrétních hodnot kategoriální proměnné odpovídá v grafu jeden sloupec, a jeho délka (nebo výška) pak udává její numerickou hodnotu.

Následující obrázek ukazuje *skládaný sloupcový graf*, v němž je každý sloupec rozdělen na segmenty odpovídající několika numerickým proměnným:

```{r setup-dnssec, code=xfun::read_utf8('../../Dashboard/R/DNSSEC_setup.R'), include=FALSE}
```

```{r dnssec-bar, code=xfun::read_utf8('../../Dashboard/R/DNSSEC_algo_domains.R'), echo=F, warning=F, message = FALSE}
```

Najetím myší na kterýkoli segment můžeme opět zjistit jeho přesné hodnoty.

Variantou sloupcového grafu je [lízátkový graf](https://datavizproject.com/data-type/lollipop-chart), který se vyznačuje lepším [poměrem dat a inkoustu](https://infovis-wiki.net/wiki/Data-Ink_Ratio) a často také pomůže odstranit [moaré](https://cs.wikipedia.org/wiki/Moar%C3%A9).

```{r dnssec-bar-lolip, echo=F, warning=F, message = FALSE, fig.height=3}

dnssec_registrars <- fromJSONwithToken(
  api_endpoint("/fred_dnssec_domains_by_registrar_latest")
) %>%
  filter(zone == "cz")
domains_registrar <- fromJSONwithToken(
  api_endpoint("/fred_domains_by_registrar_latest")
) %>%
  filter(zone == "cz")

dnssec_domains_registrars <- inner_join(dnssec_registrars,
  domains_registrar,
  by = c("registrar_id", "zone", "ts")
) %>%
  rename(dnssec_domains = domains.x, domains = domains.y) %>%
  mutate(
    percent = round(dnssec_domains / domains, 2),
    registrar_id = str_remove_all(registrar_id, "REG-"),
    registrar_id = fct_lump_n(registrar_id,
      n = 11, w = percent,
      other_level = gettext("Other")
    ),
    type_registrar = ifelse(domains >= 15000,
      "Big Registrar", "Small Registrar"
    )
  ) %>%
  group_by(registrar_id, type_registrar) %>%
  summarise(
    dnssec_domains = sum(dnssec_domains), domains = sum(domains),
    percent = mean(percent)
  )

ggplotly(dnssec_domains_registrars %>%
  filter(type_registrar == "Big Registrar") %>%
  ggplot(aes(reorder(registrar_id, percent), percent,
    text = paste0(
      registrar_id,
      "<b>\n", gettext("Total domains"), "</b>: ", domains,
      "<b>\n", gettext("DNSSEC Domains"), "</b>: ", dnssec_domains
    )
  )) +
  geom_segment(aes(xend = registrar_id, yend = 0), color = "grey") +
  geom_point(aes(x = registrar_id, y = percent),
    color = dashboard_colors[1], size = 3
  ) +
  coord_flip() +
  theme_bw() +
  scale_y_continuous(labels = scales::percent) +
  labs(x = gettext("Large Registrars"), y = ""),
tooltip = "text"
) %>%
  config(displayModeBar = FALSE) %>%
  layout(
    margin = list(t = 50),
    xaxis = list(fixedrange = TRUE)
  ) %>%
  layout(yaxis = list(fixedrange = TRUE))
```

## Sparklajny

[Sparklajny](https://www.edwardtufte.com/bboard/q-and-a-fetch-msg?msg_id=0001OR) jako třeba `r sparkline(rnorm(20), width = 120, height = 45)` jsou miniaturní grafy, které se mohou vkládat do tabulek nebo textu. Po najetí myší na čáru se opět ukáže přesná hodnota v daném bodě. Maximální a minimální hodnota zobrazeného rozsahu je vyznačena oranžovými puntíky.

## Sankeyovy diagramy

[Sankeyův diagram](https://cs.wikipedia.org/wiki/Sankey%C5%AFv_diagram) je vizualizační technika, která umožňuje zobrazit větvící se toky, případně i ve více krocích. Několik entit (uzlů) je v něm reprezentováno obdélníky s popisem. Toky mezi nimi jsou pak vyjádřeny pomocí barevných pruhů, jejichž šířka je úměrná velikosti toku.

Toto je jednoduchý příklad:

```{r sankey-example, echo=F, warning=F}

fig <- plot_ly(
  type = "sankey",
  orientation = "h",
  node = list(
    label = c("A1", "A2", "B1", "B2", "C1", "C2"),
    color = c("blue", "blue", "blue", "blue", "blue", "blue"),
    pad = 15,
    thickness = 20,
    line = list(
      color = "black",
      width = 0.5
    )
  ),
  link = list(
    source = c(0, 1, 0, 2, 3, 3),
    target = c(2, 3, 3, 4, 4, 5),
    value =  c(8, 4, 2, 8, 4, 2)
  )
)

fig <- fig %>% layout(
  title = "Basic Sankey Diagram",
  font = list(
    size = 10
  )
)


fig

```

Interaktivita je v tomto diagramu dvojího druhu:

* Po najetí myší na některou z entit (v libovolném kroku větvení) se zvýrazní toky, které z této entity vycházejí anebo do ní vstupují.
* Po najetí na jeden z pruhů se zvýrazní příslušný tok a zobrazí s ním související informace.

## Tětivový diagram

Podobně jako Sankeyův diagram se i [tětivový diagram](https://en.wikipedia.org/wiki/Chord_diagram_(mathematics)) (chord diagram) používá ke znázornění vzájemných kvantitativních vztahů v rámci množiny entit, které jsou reprezentované úseky na obvodu kruhu. Pruh (tětiva) mezi dvěma entitami představuje toky v obou směrech, a jeho šířka je znovu úměrná velikosti těchto toků.

Interaktivní vlastnosti jsou také podobné jako u Sankeyova diagramu (zkuste si sami najet myší na některou z entit nebo toků).

```{r setup-registry, code=xfun::read_utf8('../../Dashboard/R/Registry_setup.R'), include=FALSE}
```
```{r reg-transfers, echo=F, warning=F, fig.height=7, message = FALSE}
df_transfers <- fromJSON(api_endpoint("/fred_domain_transfers_1day_latest"))

day <- unique(as.Date(df_transfers$ts))
day <- format(day, locale_date_format())

df_transfers <- df_transfers %>%
  mutate(
    registrar_id_from = str_remove_all(registrar_id_from, "REG-"),
    registrar_id_to = str_remove_all(registrar_id_to, "REG-"),
    registrar_id_from = fct_lump_n(registrar_id_from,
      n = 5, ties.method = "first",
      other_level = gettext("Other")
    ),
    registrar_id_to = fct_lump_n(registrar_id_to,
      n = 5, ties.method = "first",
      other_level = gettext("Other")
    )
  ) %>%
  group_by(registrar_id_from, registrar_id_to) %>%
  summarise(domains = sum(domains))

df_transfers_remove <- df_transfers %>%
  filter(registrar_id_from == gettext("Other") &
           registrar_id_to == gettext("Other"))

df_transfers <- setdiff(df_transfers, df_transfers_remove)

f_levels <- unique(c(
  as.character(df_transfers$registrar_id_from),
  as.character(df_transfers$registrar_id_to)
))

df_transfers <- df_transfers %>%
  mutate(registrar_id_from = factor(registrar_id_from,
    levels = f_levels
  )) %>%
  mutate(registrar_id_to = factor(registrar_id_to,
    levels = f_levels
  ))

m_transfers <- unclass(xtabs(domains ~ ., df_transfers))

chorddiag(m_transfers,
  groupColors = dashboard_pal(length(f_levels)),
  showTicks = FALSE,
  groupnameFontsize = 12,
  groupnamePadding = 10,
  height = 650
)
```

## Teplotní mapa

[Teplotní mapa](https://cs.wikipedia.org/wiki/Teplotn%C3%AD_mapa) (heatmap) je grafické znázornění tabulky dat, v níž jsou hodnoty v políčcích tabulky zvýrazněny pomocí barevného odstínu nebo intenzity jasu, které odpovídají dané hodnotě. Snáze je tak možné získat celkový přehled o dvojrozměrném rozložení hodnot.

```{r reg-heatm, echo=F, warning=F, fig.height=7, message = FALSE, code=xfun::read_utf8('../../Dashboard/R/Registry_reg-transfers-month.R')}
```

## Směrnicový graf

[Směrnicový graf](https://www.betterevaluation.org/en/evaluation-options/slopegraph) (slopegraph) zobrazuje změny většího počtu proměnných v několika diskrétních krocích (obvykle časových). Je založen na poznatku, že lidé dokážou docela dobře interpretovat relativní změny mezi proměnnými porovnáním směrnic (náklonů) segmentů lomených čar. Směrnice snadno odhalí také prudké nárůsty nebo poklesy.

```{r reg-slope, echo=F, warning=F, fig.width=8, message = FALSE, code=xfun::read_utf8('../../Dashboard/R/Registry_slopegraph_registrars.R')}
```

## Kartogram

[Kartogram](https://cs.wikipedia.org/wiki/Kartogram) či choropletová mapa umožňuje zobrazit rozložení hodnot numerické nebo kategoriální proměnné v geografické mapě zemí nebo regionů. Jde o účinnou a často používanou vizualizační techniku, která ale má taky svá úskalí:

* velké regiony vytvářejí iluzi větší relativní váhy,
* barvy nemají žádný přirozený gradient, takže vizuální porovnávání hodnot vyžaduje přesouvat pozornost tam a zpět mezi mapou a legendou.

Rozlišení mapy je možné zvětšit nebo zmenšit pomocí kolečka myši, a také, jako obvykle, po najetí myší na některou zemi nebo region se zobrazí informační rámeček s podrobnými daty.

```{r setup-map, code=xfun::read_utf8('../../Dashboard/R/Traffic_setup.R'), include=FALSE}
```

```{r map, code=xfun::read_utf8('../../Dashboard/R/Traffic_server_map.R'), include=T, echo=F, warning=F, fig.align='left',fig.width=2, message=FALSE}
```

# Autentizovaný přístup

Některé grafy a statistiky nejsou veřejné. Aby se k nim uživatel dostal, musí se napřed ve webovém rozhraní přihlásit, a musí mít také autorizaci přístupu na příslušnou stránku. Autentizační a autorizační (AA) funkce jsou dostupné z menu vázaného na ikonu uživatele vpravo nahoře. Po vybrání volby **Přihlásit se** se otevře následující přihlašovací formulář:

![](../en/images/stat-login.png)

Pro přihlášení jsou k dispozici dvě možnosti:

* Standardní metodou autentizace je služba [MojeID](https://www.mojeid.cz). Po kliknutí na logo MojeID proběhne přesměrování na autentizační stránky MojeID.
* Popřípadě je také možné použít lokální uživatelské jméno a heslo. Tyto údaje poskytuje správce webových statistik.

Po úspěšném přihlášení se v uživatelském menu objeví nové možnosti:

![](images/user-menu.png)

Vybráním možnosti **Změnit heslo** se otevře formulář pro změnu hesla, volba **Odhlásit se** pak udělá přesně to, co inzeruje. Konečně, položka **Spravovat tokeny** je užitečná hlavně pro strojový přístup k REST API, což je popsáno v [API Tokeny](https://stats.nic.cz/dashboard/docs/cs/rest-api.html#tokens).
